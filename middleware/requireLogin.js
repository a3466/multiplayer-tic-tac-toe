const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { JWT_SECRET_KEY } = require("../config/keys");
const User = mongoose.model("User");

// to verify if the user (also correct user) logged in

module.exports = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({ error: "You must be logged in!!" });
  }
  const token = authorization.replace("Bearer ", "");
  jwt.verify(token, JWT_SECRET_KEY, (err, payload) => {
    if (err) {
      return res.status(401).json({ error: "You must be logged in!" });
    }
    const { _id } = payload;
    User.findById(_id).then((userData) => {
      req.user = userData;
      next();
    });
  });
};
