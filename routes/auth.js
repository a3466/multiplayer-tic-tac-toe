const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = mongoose.model("User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { JWT_SECRET_KEY } = require("../config/keys");

const requireLogin = require("../middleware/requireLogin");

router.post("/register", (req, res) => {
  const { name, userName, email, password } = req.body;
  if (!name || !email || !userName || !password) {
    return res.status(422).json({ error: "Please add all the fields!" });
  }
  bcrypt
    .hash(password, 10)
    .then((hashedPassword) => {
      User.findOne({ email: email })
        .then((savedUser) => {
          if (savedUser) {
            return res
              .status(422)
              .json({ error: "user already exists with this email!" });
          }
          const user = new User({
            name,
            userName,
            email,
            password: hashedPassword,
          });
          user
            .save()
            .then((user) => {
              return res
                .status(200)
                .json({ message: "Registered Successfully." });
            })
            .catch((error) => {
              console.log("Error while registering => ", error);
            });
        })
        .catch((err) => console.log("register router error => ", err));
    })
    .catch((err) => {
      console.log("error in bcrypt password => ", err);
    });
});

router.post("/login", (req, res) => {
  const { userName, password } = req.body;
  if (!userName || !password) {
    return res.status(422).json({ error: "Please add all the fields!" });
  }
  User.findOne({ userName: userName })
    .then((savedUser) => {
      if (!savedUser) {
        return res.status(422).json({ error: "Enter correct details!" });
      }
      bcrypt.compare(password, savedUser.password).then((match) => {
        if (match) {
          const token = jwt.sign({ _id: savedUser._id }, JWT_SECRET_KEY);
          const { _id, name, userName, email } = savedUser;
          return res
            .status(200)
            .json({ token, user: { _id, name, userName, email } });
        } else {
          return res.status(422).json({ error: "Enter correct details!" });
        }
      });
    })
    .catch((err) => console.log("login router error => ", err));
});

router.post("/user", requireLogin, (req, res) => {
  const email = req.body.email;
  console.log("email ", email);
  User.find({ email })
    .then((user) => {
      res.status(200).json({ user });
    })
    .catch((err) => {
      console.log("Error while fetching the games => ", err);
    });
});

module.exports = router;
