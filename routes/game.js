const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Game = mongoose.model("Game");
const requireLogin = require("../middleware/requireLogin");

router.get("/games", requireLogin, (req, res) => {
  const userEmail = req.user.email;
  const userId = req.user._id;
  Game.find({ $or: [{ userEmail }, { opponentId: userId }] })
    .sort({ updatedOn: 0 })
    .populate("opponentId", "_id, name")
    .then((games) => {
      res.status(200).json({ games });
    })
    .catch((err) => {
      console.log("Error while fetching the games => ", err);
    });
});

router.get("/game/:_id", requireLogin, (req, res) => {
  const _id = req.params._id;
  Game.find({ _id })
    .then((game) => {
      return res.status(200).json({ game: game });
    })
    .catch((err) => console.log("Game by id => ", err));
});

router.put("/game/:_id", requireLogin, (req, res) => {
  const _id = req.params._id;
  const { gameState, active } = req.body;
  Game.findByIdAndUpdate(
    _id,
    {
      $set: { state: gameState, active: active },
    },
    {
      new: true,
    }
  )
    .then((game) => {
      return res.status(200).json({ game: game });
    })
    .catch((err) => console.log("Game by id => ", err));
});

router.post("/game/user", requireLogin, (req, res) => {
  const { userEmail, opponentEmail, opponentId, userId } = req.body;
  if (!opponentId) {
    return res.status(422).json({ error: "Invalid user!" });
  }
  Game.find({
    $or: [
      { userEmail: userEmail, opponentId, active: true },
      { userEmail: opponentEmail, opponentId: userId, active: true },
    ],
  })
    .then((game) => {
      return res.status(200).json({ game: game });
    })
    .catch((err) => console.log(err));
});

router.post("/newgame", requireLogin, (req, res) => {
  const {
    userEmail,
    opponentId,
    state,
    active,
    opponentName,
    userName,
    updatedOn,
  } = req.body;
  if (!userEmail || !opponentId || !opponentName) {
    return res.status(422).json({ error: "Please add all the fields!" });
  }
  const game = new Game({
    userEmail,
    userName,
    opponentId,
    opponentName,
    state,
    active,
    updatedOn: new Date(),
  });
  game
    .save()
    .then((result) => {
      res.json({ game: result });
    })
    .catch((err) => {
      console.log("Error while creating new Game => ", err);
    });
});

module.exports = router;
