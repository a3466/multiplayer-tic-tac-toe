**This is a MERN stack application hosted on vercel**

## [Deployed Link] (https://asynchronous-ttt-1ovv.vercel.app/)

### Used secured deployment by not exposing the data to the client side

### Vercel environment variale are used for production side

###Assumptions

1. Once a user signed in, can not sign out
2. He will be redirected to the _games page_
3. Form validation is not added
4. Multiple users can exist with same userName and email (not checked)

### NO frame work used for CSS, all the css written from scratch

### No default router is added
