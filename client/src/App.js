import "./App.css";
import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import Register from "./components/Register";
import Login from "./components/Login";
import Games from "./components/Games";
import NewGame from "./components/NewGame";
import PlayGame from "./components/PlayGame";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/games" element={<Games />} />
        <Route path="/newgame" element={<NewGame />} />
        <Route path="/playgame/:_id" element={<PlayGame />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
