import React from "react";

const GlobalButton = ({ text, color, handleSubmit }) => {
  return (
    <button
      style={{
        border: "none",
        color: "white",
        borderRadius: "4px",
        backgroundColor: color,
        width: "100%",
        padding: "8px 0",
        boxShadow: "2px 2px 8px -3px rgba(0, 0, 0, 0.75)",
        cursor: "pointer",
      }}
      onClick={() => handleSubmit()}
    >
      {text}
    </button>
  );
};

export default GlobalButton;
