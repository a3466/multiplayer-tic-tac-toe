import React, { useState } from "react";
import GlobalButton from "./GlobalButton";
import leftArrow from "./leftarrow.png";
import { useNavigate } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";
const Login = () => {
  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [color, setColor] = useState("");
  const handleSubmit = () => {
    const url = "/login";
    fetch(url, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userName,
        password,
      }),
    })
      .then((res) =>
        res.json().then((data) => {
          if (data.error) {
            setError(data.error);
            setColor("red");
            setInterval(() => {
              setError("");
            }, 3000);
          } else {
            // console.log("token ", data.token);
            localStorage.setItem("jwt", data.token);
            localStorage.setItem("user", JSON.stringify(data.user));
            setError("");
            navigate("/");
          }
        })
      )
      .catch((err) => console.log("error while registering => ", err));
  };
  return (
    <div className="container">
      <div className="container-form">
        <div className="back-button">
          <button onClick={(e) => navigate("/")}>
            <img src={leftArrow} height={15} width={15} />
          </button>
        </div>
        <div className="content">
          <p>Login</p>
          <h2>Please enter your details</h2>
        </div>
        <div className="input-element">
          <label>Username</label>
          <input
            type="text"
            placeholder="Type your username here"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="input-element">
          <label>Password</label>
          <input
            type="text"
            placeholder="Type your password here"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
      </div>
      <div style={{ margin: "10px 0", width: "100%" }}>
        {error && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message={error} color={color} />
          </div>
        )}
        <GlobalButton
          text="Login"
          color="rgb(245 185 50)"
          handleSubmit={handleSubmit}
        />
      </div>
    </div>
  );
};

export default Login;
