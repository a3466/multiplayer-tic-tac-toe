import React, { useEffect, useState } from "react";
import GamesCard from "./GamesCard";
import { useNavigate } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";
import "./Register.css";
import "./Home.css";

const Games = () => {
  const navigate = useNavigate();
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(false);
  const jwtToken = localStorage.getItem("jwt");
  useEffect(() => {
    setLoading(true);
    fetch("/games", {
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + jwtToken,
      },
    })
      .then((res) => res.json())
      .then((res) => {
        setGames(res.games);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        localStorage.clear();
        navigate("/");
      });
  }, []);
  return (
    <div className="games">
      <h2>Your Games</h2>
      <div
        style={{
          height: "100%",
        }}
      >
        {loading ? (
          <div
            style={{
              margin: "auto 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message="Loading..." color="green" />
          </div>
        ) : games.length === 0 ? (
          <h2 style={{ fontFamily: "cursive" }}>No game found!</h2>
        ) : (
          games.map((item) => {
            return <GamesCard key={item._id} game={item} />;
          })
        )}
      </div>
      <div className="create-new-game">
        <button onClick={(e) => navigate("/newgame")}> + New Game </button>
      </div>{" "}
    </div>
  );
};

export default Games;
