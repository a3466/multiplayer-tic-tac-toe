export const checkGameResult = (state) => {
  // check rows
  if (state[0] === state[1] && state[1] === state[2]) {
    if (state[0]) {
      return state[0];
    }
  }
  if (state[3] === state[4] && state[4] === state[5]) {
    if (state[3]) return state[3];
  }
  if (state[6] === state[7] && state[7] === state[8]) {
    if (state[6]) return state[6];
  }

  // check col

  if (state[0] === state[3] && state[3] === state[6]) {
    if (state[0]) return state[0];
  }
  if (state[1] === state[4] && state[4] === state[7]) {
    if (state[1]) return state[1];
  }
  if (state[2] === state[5] && state[5] === state[8]) {
    if (state[2]) return state[2];
  }

  // check diagonal
  else if (
    (state[0] === state[4] && state[4] === state[8]) ||
    (state[2] === state[4] && state[4] === state[6])
  ) {
    if (state[4]) return state[4];
  } else {
    for (let i = 0; i < 9; i++) {
      if (!state[i]) {
        return "";
      }
    }
    return "It's a draw!";
  }
};

export const checkPlayerTurn = (game) => {
  let nullCnt = 0;
  const user = JSON.parse(localStorage.getItem("user"));
  let youMovesCnt = 0;
  game.state.map((item) => {
    if (item === null) {
      nullCnt++;
    } else if (item === user._id) {
      youMovesCnt++;
    }
  });
  const opponentCnt = 9 - nullCnt - youMovesCnt;
  if (youMovesCnt == opponentCnt) {
    if (game.opponentId != user._id) {
      return true;
    } else {
      return false;
    }
  } else if (youMovesCnt > opponentCnt) {
    return false;
  } else {
    return true;
  }
};
