import React, { useState, useEffect } from "react";
import "./Register.css";
import leftArrow from "./leftarrow.png";
import GlobalButton from "./GlobalButton";
import ErrorMessage from "./ErrorMessage";
import { useNavigate } from "react-router-dom";
const Register = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [color, setColor] = useState("");
  const handleSubmit = () => {
    const url = "/register";
    fetch(url, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        userName,
        email,
        password,
      }),
    })
      .then((res) =>
        res.json().then((data) => {
          if (data.error) {
            setError(data.error);
            setColor("red");
            setInterval(() => {
              setError("");
            }, 3000);
          } else {
            setError("");
            navigate("/login");
          }
        })
      )
      .catch((err) => console.log("error while registering => ", err));
  };

  return (
    <div className="container">
      <div className="container-form">
        <div className="back-button">
          <button>
            <img src={leftArrow} height={15} width={15} />
          </button>
        </div>
        <div className="content">
          <p>Create account</p>
          <h2>Let's get to know you better!</h2>
        </div>
        <div className="input-element">
          <label>Your name</label>
          <input
            type="text"
            placeholder="Type your name here"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="input-element">
          <label>Username</label>
          <input
            type="text"
            placeholder="Type your username here"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="input-element">
          <label>Email</label>
          <input
            type="email"
            placeholder="Type your email here"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="input-element">
          <label>Password</label>
          <input
            type="text"
            placeholder="Type your password here"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
      </div>
      <div style={{ margin: "10px 0", width: "100%" }}>
        {error && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message={error} color={color} />
          </div>
        )}
        <GlobalButton
          text="Register"
          color="rgb(245 185 50)"
          handleSubmit={handleSubmit}
        />
      </div>
    </div>
  );
};

export default Register;
