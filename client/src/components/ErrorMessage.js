import React from "react";

const ErrorMessage = ({ message, color }) => {
  return (
    <div
      style={{
        color: "white",
        borderRadius: "4px",
        backgroundColor: color,
        width: "100%",
        padding: "8px 0",
        boxShadow: "2px 2px 8px -3px rgba(0, 0, 0, 0.75)",
        display: "flex",
        justifyContent: "center",
      }}
    >
      {message}
    </div>
  );
};

export default ErrorMessage;
