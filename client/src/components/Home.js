import React, { useEffect } from "react";
import GlobalButton from "./GlobalButton";
import "./Home.css";
import { useNavigate } from "react-router-dom";
const Home = () => {
  const user = localStorage.getItem("user");
  const navigate = useNavigate();
  useEffect(() => {
    if (user) {
      navigate("/games");
    }
  }, []);
  const handleLogin = () => {
    navigate("/login");
  };
  const handleRegister = () => {
    navigate("/register");
  };
  return (
    <div className="home">
      <div className="home-top">
        <h3>async</h3>
        <h2>tic tac</h2>
        <h2>toe</h2>
      </div>
      <div style={{ margin: "5px 0", width: "100%" }}>
        <GlobalButton
          text="Login"
          color="rgb(245 185 50)"
          handleSubmit={handleLogin}
        />
      </div>
      <div style={{ margin: "5px 0", width: "100%" }}>
        <GlobalButton
          text="Register"
          color="rgba(24, 24, 233)"
          handleSubmit={handleRegister}
        />
      </div>
    </div>
  );
};

export default Home;
