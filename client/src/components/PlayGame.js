import React, { useEffect, useState } from "react";
import GlobalButton from "./GlobalButton";
import leftArrow from "./leftarrow.png";
import { useParams, useNavigate } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";
import { checkGameResult, checkPlayerTurn } from "./utils";
import "./Register.css";
import "./Home.css";

const PlayGame = () => {
  const { _id } = useParams();
  const token = localStorage.getItem("jwt");
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user"));
  const [playerTurn, setPlayerTurn] = useState(false);
  const [gameResult, setGameResult] = useState("");
  const [gameState, setGameState] = useState([
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
  ]);
  const [opponentName, setOpponentName] = useState("Player");
  const [stateId, setStateId] = useState(-1);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const getGame = async () => {
    await fetch("/game/" + _id, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then((result) => {
        setGameState(result.game[0].state);
        if (result.game[0].opponentId == user._id) {
          setOpponentName(result.game[0].userName);
        } else {
          setOpponentName(result.game[0].opponentName);
        }
        setPlayerTurn(checkPlayerTurn(result.game[0]));
        setGameResult(checkGameResult(result.game[0].state));
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    setLoading(true);
    getGame();
    setLoading(false);
  }, []);

  const handleMove = (id) => {
    if (gameState[id] !== null) {
      setError("Invalid Move!");
      return;
    } else {
      setStateId(id);
    }
  };

  const handleSubmit = () => {
    setLoading(true);
    let newState = gameState;
    newState[stateId] = user._id;
    setGameState(newState);
    let active = true;
    if (checkGameResult(newState)) {
      active = false;
    }
    fetch("/game/" + _id, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({ gameState, active }),
    })
      .then((res) => res.json())
      .then((result) => {
        setGameState(result.game.state);
        if (result.game.opponentId == user._id) {
          setOpponentName(result.game.userName);
        } else {
          setOpponentName(result.game.opponentName);
        }
        setPlayerTurn(checkPlayerTurn(result.game));
        setGameResult(checkGameResult(result.game.state));
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="container">
      <div className="container-form">
        <div className="back-button">
          <button
            style={{ cursor: "pointer" }}
            onClick={(e) => navigate("/games")}
          >
            <img src={leftArrow} height={15} width={15} />
          </button>
        </div>
        <h2 style={{ display: "flex", justifyContent: "left" }}>
          Game with {opponentName}
        </h2>
        <p>Your Piece</p>
        <div className="cross-img">&#xd7;</div>
        {!gameResult ? (
          playerTurn ? (
            <div className="player-turn">Your move</div>
          ) : (
            <div className="player-turn">Thier move</div>
          )
        ) : gameResult == user._id ? (
          <div className="player-turn">You won!</div>
        ) : (
          <div className="player-turn">Opponent won</div>
        )}
      </div>
      <div className="move-grid">
        {gameState.map((item, id) => {
          if (id == stateId && playerTurn) {
            return (
              <button className="cross-img" key={id}>
                &#xd7;
              </button>
            );
          } else if (item === user._id) {
            return (
              <button className="cross-img" key={id}>
                &#xd7;
              </button>
            );
          } else if (item) {
            return (
              <button className="zero-img" key={id}>
                O
              </button>
            );
          } else {
            return (
              <button
                style={{ width: "4.5rem", height: "4.5rem" }}
                className=""
                key={id}
                onClick={(e) => handleMove(id)}
              ></button>
            );
          }
        })}
      </div>
      <div style={{ margin: "10px 0", width: "100%" }}>
        {error && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message={error} color="red" />
          </div>
        )}
        {loading && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message="Loading..." color="green" />
          </div>
        )}
        {!gameResult ? (
          playerTurn ? (
            <GlobalButton
              text="Submit"
              color="rgb(245 185 50)"
              handleSubmit={handleSubmit}
            />
          ) : (
            <ErrorMessage
              message={"Waiting for " + opponentName}
              color="rgba(0,0,0,0.25)"
            />
          )
        ) : gameResult == user._id ? (
          <div className="player-turn">You won!</div>
        ) : (
          <div className="player-turn">Opponent won</div>
        )}
      </div>
    </div>
  );
};

export default PlayGame;
