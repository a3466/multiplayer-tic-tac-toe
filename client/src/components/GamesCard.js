import React, { useEffect, useState } from "react";
import GlobalButton from "./GlobalButton";
import { useNavigate } from "react-router-dom";
import { checkGameResult, checkPlayerTurn } from "./utils";

// time zone

const GamesCard = ({ game }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate("/playgame/" + game._id);
  };
  const user = JSON.parse(localStorage.getItem("user"));
  const [playerTurn, setPlayerTurn] = useState("");
  const [gameResult, setGameResult] = useState("");
  const [dateTime, setDateTime] = useState(new Date().toLocaleString());
  useEffect(() => {
    setPlayerTurn(checkPlayerTurn(game));
    setGameResult(checkGameResult(game.state));
    setDateTime(new Date(game.updatedOn).toLocaleString());
  }, []);
  return (
    <div className="games-card">
      <h3>
        Game with{" "}
        {game.opponentId._id == user._id ? game.userName : game.opponentName}
      </h3>
      {gameResult ? (
        gameResult == user._id ? (
          <p>You won!</p>
        ) : (
          <p>Opponent won</p>
        )
      ) : !playerTurn ? (
        <>
          <p>You've made your move</p>
          <p>Waiting for them</p>
        </>
      ) : (
        <>
          <p>
            {game.opponentId._id == user._id
              ? game.userName + " just made their move!"
              : game.opponentName + " just made their move!"}
          </p>
          <p>It's your turn to play game</p>
        </>
      )}
      <span>{dateTime}</span>
      <div style={{ margin: "12px 0 4px 0", width: "100%" }}>
        <GlobalButton
          text={game.active && playerTurn ? "Play" : "View game"}
          color="rgb(245 185 50)"
          handleSubmit={handleClick}
        />
      </div>
    </div>
  );
};

export default GamesCard;
