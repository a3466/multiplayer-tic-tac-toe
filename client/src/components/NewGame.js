import React, { useState, useEffect } from "react";
import leftArrow from "./leftarrow.png";
import GlobalButton from "./GlobalButton";
import { useNavigate } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";
const NewGame = () => {
  const navigate = useNavigate();
  const [gameState, setGameState] = useState([
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null,
  ]);
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const [opponent, setOpponent] = useState({});

  const handleSubmit = () => {
    if (!email) {
      setError("Please enter the correct email!");
      return;
    }
    const url = "/newgame";
    const user = JSON.parse(localStorage.getItem("user"));
    const userId = user._id;
    const userEmail = user.email;
    setLoading(true);
    fetch("/user", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      body: JSON.stringify({ email }),
    })
      .then((res) => res.json())
      .then((result) => {
        if (result.user.length === 0) {
          setError("No user with this email!");
          setLoading(false);
          return;
        } else if (result.user[0]._id === user._id) {
          setLoading(false);
          setError("You can not start game with yourself!");
          return;
        } else {
          setOpponent(result.user[0]);
          fetch("/game/user", {
            method: "post",
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + localStorage.getItem("jwt"),
            },
            body: JSON.stringify({
              userEmail: userEmail,
              opponentId: result.user[0]._id,
              opponentEmail: email,
              userId,
            }),
          })
            .then((res) => res.json())
            .then((resGame) => {
              if (resGame.game.length > 0) {
                console.log("resGame", resGame);
                setError("You have an active game with this user!");
                setLoading(false);
                return;
              } else {
                setError("");
                fetch(url, {
                  method: "post",
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + localStorage.getItem("jwt"),
                  },
                  body: JSON.stringify({
                    userEmail,
                    userName: user.name,
                    opponentId: result.user[0]._id,
                    opponentName: result.user[0].name,
                    state: gameState,
                  }),
                })
                  .then((res) =>
                    res.json().then((data) => {
                      if (data.error) {
                        console.log(data.error);
                      } else {
                        navigate("/playgame/" + data.game._id);
                      }
                    })
                  )
                  .catch((err) =>
                    console.log("error while registering => ", err)
                  );
              }
            })
            .catch((err) => console.log(err));
        }
        setLoading(false);
      });
  };
  return (
    <div className="container">
      <div className="container-form">
        <div className="back-button">
          <button onClick={(e) => navigate("/games")}>
            <img src={leftArrow} height={15} width={15} />
          </button>
        </div>
        <div className="content">
          <p>Start a new game</p>
          <h2>Whom do you want to play with?</h2>
        </div>
        <div className="input-element">
          <label>Email</label>
          <input
            type="email"
            placeholder="Type your email here"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
      </div>
      <div style={{ margin: "10px 0", width: "100%" }}>
        {error && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message={error} color="red" />
          </div>
        )}
        {loading && (
          <div
            style={{
              margin: "10px 0",
              width: "100%",
              animation: "ease-in-out",
            }}
          >
            <ErrorMessage message="Loading..." color="green" />
          </div>
        )}
        <GlobalButton
          text="Start game"
          color="rgb(245 185 50)"
          handleSubmit={handleSubmit}
        />
      </div>
    </div>
  );
};

export default NewGame;
