const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema.Types;
const gameSchema = new mongoose.Schema({
  userEmail: {
    type: String,
    require: true,
  },
  userName: {
    type: String,
    require: true,
  },
  opponentId: {
    type: ObjectId,
    ref: "User",
    require: true,
  },
  opponentName: {
    type: String,
    require: true,
  },
  state: {
    type: Array,
    require: true,
  },
  active: {
    type: Boolean,
    require: true,
    default: true,
  },
  updatedOn: {
    type: Date,
    require: true,
    default: new Date(),
  },
});

mongoose.model("Game", gameSchema);
