const express = require("express");
const app = express();
const PORT = 8000;

const mongoose = require("mongoose");
const { MONGOURL } = require("./config/keys");
mongoose.connect(MONGOURL);
mongoose.connection.on("connected", () => {
  console.log("conncted to mongo...");
});
mongoose.connection.on("error", (err) => {
  console.log("error connecting to mongodb => ", err);
});

require("./models/user");
require("./models/game");

//middlewares
app.use(express.json()); // parse incoming request to json
app.use(require("./routes/auth")); // register routes
app.use(require("./routes/game"));

if (process.env.NODE_ENV == "production") {
  const path = require("path");
  app.get("/", (req, res) => {
    app.use(express.static(path.resolve(__dirname, "client", "build")));
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

app.listen(PORT, () => {
  console.log("server is running on ", PORT);
});
